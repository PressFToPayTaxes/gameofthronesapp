﻿using Newtonsoft.Json;

namespace GameOfThronesApp
{
    public class Pagerank
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("rank")]
        public int Rank { get; set; }
    }
}
