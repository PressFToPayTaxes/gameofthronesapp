﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameOfThronesApp
{
    /// <summary>
    /// Логика взаимодействия для CharacterInfo.xaml
    /// </summary>
    public partial class CharacterInfo : Window
    {
        public CharacterInfo()
        {
            InitializeComponent();
        }

        public CharacterInfo(Character character)
        {
            InitializeComponent();

            if (character.Image != null)
            {
                ImageSourceConverter converter = new ImageSourceConverter();
                characterImage.Source = converter.ConvertFrom(character.Image) as ImageSource;
            }

            idTextBlock.Text = "Id: " + character.Id;
            nameTextBlock.Text = "Name: " + character.Name;
            titlesTextBlock.Text = "Titles: ";
            foreach (var title in character.Titles)
                titlesTextBlock.Text += title + ", ";

            spouseTextBlock.Text = "Spouse: ";
            foreach (var spouse in character.Spouse)
                spouseTextBlock.Text += spouse + ", ";

            childrenTextBlock.Text = "Children: ";
            foreach (var child in character.Children)
                spouseTextBlock.Text += child + ", ";

            allegianceTextBlock.Text = "Allegiance: ";
            if (character.Allegiance != null)
                foreach (var allegiance in character.Allegiance)
                    allegianceTextBlock.Text += allegiance + ", ";
            else
                allegianceTextBlock.Text += "-";

            booksTextBlock.Text = "Books: ";
            foreach (var book in character.Books)
                booksTextBlock.Text += book + ", ";

            plodTextBlock.Text = "Predicted likelihood of death: " + character.PredictedLikelihoodOfDeath;
            slugTextBlock.Text = "Slug: " + character.Slug;
            genderTextBlock.Text = "Gender: " + character.Gender;
            cultureTextBlock.Text = "Culture: " + character.Culture;
            houseTextBlock.Text = "House: " + character.House;
            isAliveTextBlock.Text = "Alive: " + character.IsAlive.ToString();
            birthTextBlock.Text = "Birth: " + character.Birth;
            placeOfDeathTextBlock.Text = "Place of death: " + character.PlaceOfDeath;
            deathTextBlock.Text = "Death: " + character.Death;
            placeOfBirthTextBlock.Text = "Place of birth: " + character.PlaceOfBirth;
            motherTextBlock.Text = "Mother: " + character.Mother;
            fatherTextBlock.Text = "Father: " + character.Father;
            heirTextBlock.Text = "Heir: " + character.Heir;
        }
    }
}
