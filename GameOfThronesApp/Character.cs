﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesApp
{
    public class Character
    {
        [JsonProperty("titles")]
        public List<string> Titles { get; set; }
        [JsonProperty("spouse")]
        public List<string> Spouse { get; set; }
        [JsonProperty("children")]
        public List<string> Children { get; set; }
        [JsonProperty("allegiances")]
        public List<string> Allegiance { get; set; }
        [JsonProperty("books")]
        public List<string> Books { get; set; }
        [JsonProperty("plod")]
        public double PredictedLikelihoodOfDeath { get; set; }
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("Image")]
        public string Image { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("culture")]
        public string Culture { get; set; }
        [JsonProperty("house")]
        public string House { get; set; }
        [JsonProperty("alive")]
        public bool IsAlive { get; set; }
        [JsonProperty("birth")]
        public int? Birth { get; set; }
        [JsonProperty("placeOfDeath")]
        public string PlaceOfDeath { get; set; }
        [JsonProperty("death")]
        public int? Death { get; set; }
        [JsonProperty("placeOfBirth")]
        public string PlaceOfBirth { get; set; }
        [JsonProperty("father")]
        public string Father { get; set; }
        [JsonProperty("mother")]
        public string Mother { get; set; }
        [JsonProperty("heir")]
        public string Heir { get; set; }
    }
}
