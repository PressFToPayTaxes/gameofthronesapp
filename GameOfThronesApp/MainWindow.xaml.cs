﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using CefSharp.Wpf;

namespace GameOfThronesApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            
        }
        
        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var characterName = button.Content as string;

            var currentCharacter = new Character();
            foreach(var character in _searchedCharacters)
            {
                if (character.Name == characterName)
                    currentCharacter = character;
            }

            CharacterInfo infoWindow = new CharacterInfo(currentCharacter);
            infoWindow.Show();
        }

        private List<Character> _allCharacters = new List<Character>();
        private List<Character> _searchedCharacters = new List<Character>();

        private void SearchingTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            if (searchingTextBox.Text == string.Empty)
                _searchedCharacters = _allCharacters;

            searchedCharactersListBox.Items.Clear();
            foreach (var character in _searchedCharacters)
            {
                if (character.Name.ToLower().Contains(searchingTextBox.Text.ToLower()))
                {
                    var button = new Button
                    {
                        Content = character.Name,
                        Background = Brushes.DarkBlue
                    };
                    button.Click += ButtonClick;

                    searchedCharactersListBox.Items.Add(button);
                }
            }
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Content-Type", "text/json");
                var result = client.DownloadString("https://api.got.show/api/book/characters");

                _allCharacters = JsonConvert.DeserializeObject<List<Character>>(result);
            }
            _searchedCharacters = _allCharacters;

            foreach (var character in _searchedCharacters)
            {
                var button = new Button
                {
                    Content = character.Name,
                    Background = Brushes.DarkBlue
                };
                button.Click += ButtonClick;

                searchedCharactersListBox.Items.Add(button);
            }
        }
    }
}
